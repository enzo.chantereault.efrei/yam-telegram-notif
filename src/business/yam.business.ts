import { Subject } from "rxjs";
import { YamRepository } from "../repository/index.repository";
import { OrderType, StatusResponse, TokenInfo, YamOffer, YamResponse } from "../shared/index.shared";
import { RealtBusiness } from "./realt.business";


export class YamBusiness {
    yamRepo: YamRepository;

    constructor() {
        this.yamRepo = new YamRepository();
    }

    createOfferListener(offerSubject: Subject<YamOffer>): void {
        this.yamRepo.createOfferListener(offerSubject);
    }

    isValidOffer(yamOffer: YamOffer): boolean {
        if(yamOffer.priceDifference == undefined) {
            return false;
        }

        let isValid: boolean = !yamOffer.isPrivate && yamOffer.quantity > 0;
        if(yamOffer.type == OrderType.BUY_ORDER){
            isValid = isValid && (yamOffer.priceDifference! >= 0)
        } else {
            isValid = isValid && (yamOffer.priceDifference! <= 0)
        }
        return isValid;
    }

    async getOfferById(offerId: number): Promise<YamResponse> {
        const count: number = await this.yamRepo.getOfferCount();
        if(offerId > count - 1) {
            return new YamResponse(StatusResponse.ID_ERROR);
        }

        return await this.yamRepo.getOfferById(offerId);
    }

    async getRealtTokenPrice(token: TokenInfo): Promise<number | undefined> {
        if(!token.isRealtToken){
            return;
        }

        const realtBu: RealtBusiness = new RealtBusiness();

        return +(await realtBu.getRealtToken(token.contractAddress)).tokenPrice;
    }

    async setOfferPriceDifference(offer: YamOffer): Promise<YamOffer> {
        const ipoBidTokenPrice: number | undefined = await this.getRealtTokenPrice(offer!.bidToken);
        const ipoAskTokenPrice: number | undefined = await this.getRealtTokenPrice(offer!.askToken);
        if(ipoBidTokenPrice && !ipoAskTokenPrice){
            offer!.priceDifference = (offer!.unitPrice - ipoBidTokenPrice)/ipoBidTokenPrice;
        } else if(!ipoBidTokenPrice && ipoAskTokenPrice) {
            offer!.priceDifference = ((offer!.unitPrice) - ipoAskTokenPrice)/ipoAskTokenPrice;
        }
        
        return offer;
    }
}