import { RealtRepository } from "../repository/index.repository";

export class RealtBusiness {
    realtRepo: RealtRepository;

    constructor() {
        this.realtRepo = new RealtRepository();
    }

    public getRealtToken(contractAddress: string): Promise<any> {
        return this.realtRepo.getRealtToken(contractAddress);
    }
}
