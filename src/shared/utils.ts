import { ethers } from "ethers";

export class Utils {
    
    static hexToDecimal(hex: string): number {
        return parseInt(hex, 16);
    }
    
    static formatUnits(num: number, decimals: number): number {
        return +ethers.utils.formatUnits(num, decimals)
    }

    static round(num: number, decimals: number): number {
        return Number(Math.round(Number(num + "e" + decimals)) + "e-" + decimals);
    }
}
