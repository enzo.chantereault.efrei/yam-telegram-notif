import { Utils } from "../../utils";
import { OrderType } from "../order-type.model";
import { TokenInfo } from "../token-info.model";

export class YamOffer {
    id!: number;
    type: OrderType;
    bidToken!: TokenInfo;
    askToken!: TokenInfo;
    unitPrice!: number;
    quantity!: number;
    isPrivate!: boolean;
    priceDifference?: number;

    private constructor(init: Partial<YamOffer>) {
        Object.assign(this, init);
        let orderType: OrderType = OrderType.SWAP_ORDER;
        if(!init?.bidToken?.isRealtToken && init?.askToken?.isRealtToken) {
            orderType = OrderType.BUY_ORDER;
            this.quantity = (init?.quantity!/init?.unitPrice!);
            this.unitPrice = (1/init?.unitPrice!);
        } else if(init?.bidToken?.isRealtToken && !init?.askToken?.isRealtToken) {
            orderType = OrderType.SELL_ORDER;
        }
        this.type = orderType;
    }

    static async initialize(offerId: number, buyer: string, offerToken: string, buyerToken: string, price: number, amount: number) {
        const id: number = Utils.formatUnits(offerId, 0);
        const isPrivate: boolean = Utils.hexToDecimal(buyer) != 0;
        const bidToken: TokenInfo = await TokenInfo.fetchTokenInfo(offerToken);
        const askToken: TokenInfo = await TokenInfo.fetchTokenInfo(buyerToken);
        const unitPrice: number = Utils.formatUnits(price, askToken.decimals);
        const quantity: number = Utils.formatUnits(amount, bidToken.decimals);

        return new YamOffer({
            id: id,
            bidToken: bidToken,
            askToken: askToken,
            unitPrice: unitPrice,
            quantity: quantity,
            isPrivate: isPrivate
        });
    }
};