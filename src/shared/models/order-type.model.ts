export enum OrderType {
    BUY_ORDER = 1,
    SELL_ORDER = 2,
    SWAP_ORDER = 3
};

export const orderTypes: Record<OrderType, string> = {
    1: "purchase",
    2: "sale",
    3: "swap"
}