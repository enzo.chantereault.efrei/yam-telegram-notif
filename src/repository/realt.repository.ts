import axios from "axios";
import { realtAPI } from "../env/index.env";


export class RealtRepository {

    constructor() {}

    public getRealtToken(contractAddress: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios.get<any>(`${realtAPI}/v1/token/${contractAddress}`)
            .then(
                (response) => {
                    resolve(response.data);
                }
            )
            .catch(
                (error) => {
                    console.log(error);
                    reject();
                }
            )
        })
    }
}
