import { Subject } from "rxjs";
import { 
    StatusResponse,
    Utils, 
    YamOffer, 
    YamResponse } from "../shared/index.shared";
import { contract } from "../env/index.env";

export class YamRepository {

    constructor() {}

    async getOfferCount(): Promise<number> {
        const count: {_hex: string, _isBigNumber: boolean} = await contract.getOfferCount();
        return Utils.hexToDecimal(count._hex);
    }
    
    async getOfferById(offerId: number): Promise<YamResponse> {
        try {
            const [offerToken, buyerToken, seller, buyer, price, amount] = await contract.showOffer(offerId);
            const offerDetails = await YamOffer.initialize(offerId, buyer, offerToken, buyerToken, price, amount);
            return new YamResponse(StatusResponse.OK, offerDetails);
        } catch(error: any) {
            console.log("Error: ", error.reason);
            return new YamResponse(StatusResponse.UNKNOWN_ERROR);
        }
    }

    createOfferListener(offerSubject: Subject<YamOffer>): void {
        contract.on("OfferCreated",
            async (
                offerToken: string,
                buyerToken: string,
                seller: string,
                buyer: string,
                offerId: number,
                price: number,
                amount: number,
                event: any
            ) => offerSubject.next(await YamOffer.initialize(offerId, buyer, offerToken, buyerToken, price, amount))
        );
    }

}