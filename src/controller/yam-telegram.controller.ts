
import { Context, Telegraf } from 'telegraf';
import { ENVIRONMENT } from "../env/environment";
import { debounceTime, Subject } from "rxjs";
import moment from 'moment';
import { YamBusiness } from '../business/index.business';
import { 
    OrderType, 
    orderTypes, 
    Queue, 
    StatusResponse, 
    statusResponseMessages, 
    Utils, 
    YamOffer, 
    YamResponse } from '../shared/index.shared';

export class YamTelegramController {

    yamBu: YamBusiness;
    bot: Telegraf<Context>;
    chatId: number = 0;
    offerSubject$: Subject<YamOffer>;
    queueSubject$: Subject<Queue<string>>;
    queue: Queue<string>;
    alertMessageConfig: object = {
        parse_mode: "HTML",
        disable_notification: false,
        disable_web_page_preview: true
    };

    constructor() {
        this.yamBu = new YamBusiness();
        this.bot = new Telegraf<Context>(ENVIRONMENT.TELEGRAM_TOKEN);
        this.offerSubject$ = new Subject<YamOffer>();
        this.queueSubject$ = new Subject<Queue<string>>();
        this.queue = new Queue(this.queueSubject$);
        this.bot.start((ctx) => {
            this.chatId = ctx.chat.id;
            this.yamBu.createOfferListener(this.offerSubject$);
            ctx.reply(`👀 YAM trading bot started at ${moment(new Date()).format("DD-MM-YYYY")}`);
            console.log(`\nListening to YAM started at ${moment(new Date()).format("DD-MM-YYYY")}`);
        });
        this.startAlertes();
        this.startComands();
        this.bot.launch();
        // Enable graceful stop
        process.once('SIGINT', () => this.bot.stop('SIGINT'));
        process.once('SIGTERM', () => this.bot.stop('SIGTERM'));
    }

    offerHeader = (marketName: string, offer: YamOffer): string => {
        let str: string = `<b><i>~${marketName.toUpperCase()}~</i></b>`
        + `\n\n${this.yamBu.isValidOffer(offer) ? '🟢' : '🔴'} `
        +`<a href="${ENVIRONMENT.YAM_WEBSITE}">${orderTypes[offer.type].toUpperCase()} offer ${offer.id}</a> `
        + (
            offer.isPrivate
            ? `🔒`
            : ""
        );
        return str;
    }

    offerBody = (offer: YamOffer): string => {
        let str: string = "\n<pre>";
        if(offer.type == OrderType.BUY_ORDER) {
            str += `${offer.askToken.name}/${offer.bidToken.name}`;
        } else {
            str += `${offer.bidToken.name}/${offer.askToken.name}`;
        }
    
        str += `\nPrice: ${Utils.round(offer.unitPrice, 2)} `
        +(
            offer.priceDifference 
            ? `(${ (offer.priceDifference >= 0 ? "+" : "") + Utils.round(offer.priceDifference, 2) * 100}%)`
            : ''
        )
        +`\nQuantity: ${offer.quantity}</pre>`;
    
        return str;
    }

    offerFooter = (offer: YamOffer): string => {
        let str: string = `\n`
        + (
            offer.bidToken.isRealtToken 
            ? `\n<a href="${offer.bidToken.getRealtLink()}">ℹ ${offer.bidToken.name}</a>` 
            : ""
        )
        + (
            offer.askToken.isRealtToken 
            ? `\n<a href="${offer.askToken.getRealtLink()}">ℹ️ ${offer.askToken.name}</a>` 
            : ""
        );
        return str;
    }

    offerMessage = (marketName: string, offer: YamOffer): string => {
        let str: string = this.offerHeader(marketName, offer) 
        + this.offerBody(offer)
        + this.offerFooter(offer);

        return str;
    }

    reactionToOfferMessage = (offer: YamOffer): string => {
        const isValid: boolean = this.yamBu.isValidOffer(offer);
        if(!isValid) {
            return `Offer ${offer.id} => 🙅‍♂️ nothing to see here...`;
        }

        let message: string = "";
        if(offer.type == OrderType.BUY_ORDER) {
            message = `Offer ${offer.id} => 🤝 Sold !`
        } else {
            message = `Offer ${offer.id} => 🤝 Bought !`;
        }
        
        return message;
    }

    sendTelegram(message: string): void {
        this.queue.enqueue(message);
    };

    startAlertes(): void {
        this.queueSubject$.pipe(debounceTime(500)).subscribe({
            next: (queue: Queue<string>) => {
                const message = queue.dequeue();
                if(!message) {
                    return;
                }
                console.log(`Bot sent: `, message);
                this.bot.telegram.sendMessage(
                    this.chatId, 
                    message ?? "", 
                    this.alertMessageConfig
                );
            }
        });

        this.offerSubject$.subscribe({
            next: async (incomingYamOffer: YamOffer) => {
                incomingYamOffer = await this.yamBu.setOfferPriceDifference(incomingYamOffer);
                this.sendTelegram(this.offerMessage("YAM", incomingYamOffer));
            }
        });
    }

    startComands(): void {
        this.bot.command("getOffer", async (ctx, next) => {
            let input: string[] = ctx.message.text.split(" ");

            if(input.length == 1) {
                const message: string = "🆘 Missing <i>id</i> (number). see exemple:" 
                + `\n<pre>/getOffer 1 2 3</pre>`;
                this.sendTelegram(message);
                return;
            }

            const idArray: string[] = input.slice(1);
            for (let index = 0; index < idArray.length; index++) {
                const id: number = +idArray[index];
                if(Number.isNaN(id)) {
                    const message: string = `Offer ${idArray[index]}`
                    +`\n❌ <i>${idArray[index]}</i> is not a number. see exemple:`
                    + `\n<pre>/getOffer 1 2 3</pre>`;
                    this.sendTelegram(message);
                    continue;
                }

                const response: YamResponse = await this.yamBu.getOfferById(id);
                if(response.status != StatusResponse.OK) {
                    const message: string = `Offer ${id}`
                    +`\n❌ ${statusResponseMessages[response.status]}`;
                    this.sendTelegram(message);
                    continue;
                }

                response.yamOffer = await this.yamBu.setOfferPriceDifference(response.yamOffer!);
                const message: string = this.offerMessage("Yam", response.yamOffer);
                this.sendTelegram(message);
                this.sendTelegram(this.reactionToOfferMessage(response.yamOffer));
            }
        });
    }

}